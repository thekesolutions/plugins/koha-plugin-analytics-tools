# koha-plugin-analytics-tools

## Introduction

This plugin intends to provide additional tools for transitioning into
a more robust analytics handling in Koha.

The heavy-lifting work is being done in the context of [bug 15851](https://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=15851)
which will be the definite solution.

## Tools

### scripts/fix_001.pl

This script takes care or making sure the _001_ field contains the biblionumber, unless it
already contains an identifier. This can be forced. In order to run it, you need to be in
your instance's shell:

`
   $ koha-shell <instance>
  k$ cd ~
  k$ PERL5LIB=. perl Koha/Plugin/Com/Theke/AnalyticsTools/scripts/fix_001.pl --help
`

In order to run as a cronjob, you should use the full path like:

`
    0 0 * * * root koha-foreach --chdir "cd plugins; PERL5LIB=/usr/share/koha/lib:. perl Koha/Plugin/Com/Theke/AnalyticsTools/scripts/fix_001.pl --quiet --days 2"
`

Note that we pass _--quiet_, but found errors will still be sent to STDERR so they will be notified
by the cron system.