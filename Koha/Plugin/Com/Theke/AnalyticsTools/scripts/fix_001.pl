#!/usr/bin/perl

# Copyright 2020 Theke Solutions
#
# This file is part of koha-plugin-analytics-tools.
#
# koha-plugin-analytics-tools is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# koha-plugin-analytics-tools is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with koha-plugin-analytics-tools; if not, see <http://www.gnu.org/licenses>.

use Modern::Perl;

use Getopt::Long;

use Koha::Biblios;
use Koha::DateUtils qw(dt_from_string);
use Koha::Database;

use C4::Biblio qw(GetMarcBiblio ModBiblio);

binmode STDOUT, ':encoding(UTF-8)';
binmode STDERR, ':encoding(UTF-8)';

my $help;
my $quiet;
my $force;
my $days;

GetOptions(
    'h|help'  => \$help,
    'q|quiet' => \$quiet,
    'force'   => \$force,
    'days=i'  => \$days,
);
my $usage = << 'ENDUSAGE';

This script fixes bibliographic records in Koha so the 001 field contains
the biblionumber. The default behaviour is to keep the current value if present.

This script has the following parameters :
    -h --help     This message
    --force       Overwrite the 001 field
    --days N      Only work on records modified up to N days ago
    -q --quiet    Quiet output

ENDUSAGE

if ( $help ) {
    print $usage;
    exit;
}

my $dtf = Koha::Database->new->schema->storage->datetime_parser;

my $counter  = 0;
my $modified = 0;
my $skipped  = 0;
my $failed   = 0;

my $query = {};

if ($days) {
    $query->{timestamp} =
      { '>=' => $dtf->format_datetime( dt_from_string->subtract( days => $days ) ) };
}

my $biblios_rs = Koha::Biblios->search( $query );

while ( my $biblio = $biblios_rs->next ) {

    $counter++;

    my $record = GetMarcBiblio({ biblionumber => $biblio->biblionumber });

    unless ( $record ) {
        print STDERR "FAIL: Skipping ". $biblio->biblionumber . "(problem fetching record)\n";
        $failed++;
        next;
    }

    my @fields_001 = $record->field('001');

    my $was_modified = 0;
    my $overwritten = 0;

    if ( scalar @fields_001 > 1 ) {
        print STDERR "FAIL: Skipping ". $biblio->biblionumber . "(more than one 001 field)\n";
        $failed++;
        next;
    }

    my $field_001 = $fields_001[0];

    if ( defined $field_001 and $field_001->data and $force ) {
        $field_001->update( $biblio->biblionumber . '' );
        $was_modified    = 1;
        $overwritten = 1;
    }
    elsif ( !defined $field_001 or ( defined $field_001 and !$field_001->data ) ) {

        $record->insert_fields_ordered(
            MARC::Field->new( '001', $biblio->biblionumber . '' )
        );

        $was_modified = 1;
    }
    else {
        $skipped++;
    }

    ModBiblio( $record, $biblio->biblionumber, $biblio->frameworkcode )
       if $was_modified;

    unless ( $quiet ) {
        if ( $was_modified ) {
            if ( $overwritten ) {
                print STDOUT $biblio->biblionumber . ":\t overwritten\n";
            }
            else {
                print STDOUT $biblio->biblionumber . ":\t  added (missing)\n";
            }
        }
        else {
            print STDOUT $biblio->biblionumber . ":\t  unchanged\n";
        }
    }

    $modified++
        if $was_modified;
}

unless ( $quiet ) {
    print STDOUT "=============================\n";
    print STDOUT "  Total:    $counter\n";
    print STDOUT "  Modified: $modified\n";
    print STDOUT "  Skipped:  $skipped\n";
    print STDOUT "  Errors:   $failed\n";
    print STDOUT "=============================\n";
}

1;
