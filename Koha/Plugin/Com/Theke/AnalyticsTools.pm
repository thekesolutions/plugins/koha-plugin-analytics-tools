package Koha::Plugin::Com::Theke::AnalyticsTools;

# Copyright 2020 Theke Solutions
#
# This file is part of koha-plugin-analytics-tools.
#
# koha-plugin-analytics-tools is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# koha-plugin-analytics-tools is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with koha-plugin-analytics-tools; if not, see <http://www.gnu.org/licenses>.

use Modern::Perl;
use utf8;

use base qw(Koha::Plugins::Base);

our $VERSION = "{VERSION}";

our $metadata = {
    name            => 'Analytics Tools for Koha',
    author          => 'Tomas Cohen Arazi',
    description     => 'Provides tools for dealing with analytics in Koha',
    date_authored   => '2020-05-20',
    date_updated    => '2020-05-20',
    minimum_version => '17.0500000',
    maximum_version => undef,
    version         => $VERSION,
};

=head1 METHODS

=cut

sub new {
    my ( $class, $args ) = @_;

    ## We need to add our metadata here so our base class can access it
    $args->{'metadata'} = $metadata;

    my $self = $class->SUPER::new($args);

    return $self;
}

1;
